﻿var uID;

function signIn() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
}

function createNewPost(name, email, photo, uid) {
    firebase.database().ref('users/' + uid).set({
        name: name,
        email: email,
        photo: photo,
        uid: uid
    });
}

firebase.auth().getRedirectResult().then(function (result) {
    if (result.credential) {
        var token = result.credential.accessToken
    }
    // The signed-in user info.
    if (user != null) {
        console.log("name: " + user.displayName);
        console.log("email: " + user.email);
        console.log("photoUrl: " + user.photoURL);
        console.log("uid: " + user.uid);
    }

}).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;

});

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        var name = user.displayName;
        var email = user.email;
        var photo = user.photoURL;
        var uid = user.uid;
        uID = email.split("@");
        uID = uID[0].replace(/[.]/g,"");
        createNewPost(name, email, photo, uid);
        console.log("name: " + user.displayName);
        console.log("email: " + user.email);
        console.log("photoUrl: " + user.photoURL);
        console.log("uid: " + user.uid);
        console.log("adres: " + document.URL);
        if (document.URL == "http://localhost:4400/index.html" || document.URL == "file:///android_asset/www/index.html")
        {
            window.location.href = "home.html";
        }
    }
});

function signOut() {
    firebase.auth().signOut().then(function () {
        // Sign-out successful.
        window.location.href = "index.html";
    }, function (error) {
        console.log(error);
    });
}






