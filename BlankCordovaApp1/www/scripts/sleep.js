﻿document.getElementById("resultTime").defaultValue = "1";
document.getElementById("resultAmount").defaultValue = "0";
document.getElementById("resultAwakenings").defaultValue = "0";
$(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var first = true;

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("sleepLogDate").value = today;
});
var r,rate = 0;

document.getElementById("butTime").addEventListener("click", function () { r = document.getElementById('resultTime'); startRecognition(); }, false);
document.getElementById("butAmount").addEventListener("click", function () { r = document.getElementById('resultAmount'); startRecognition(); }, false);
document.getElementById("butAwakenings").addEventListener("click", function () { r = document.getElementById('resultAwakenings'); startRecognition(); }, false);

$(function () {

    $("#rateYo1").on("rateyo.init", function () { console.log("rateyo.init"); });

    $("#rateYo1").rateYo({

        numStars: 5,
        starWidth: "25px",
        spacing: "5px",
        fullStar: true,

        onInit: function () {

            console.log("On Init");
        },
        onSet: function () {

            console.log("On Set");
        }
    }).on("rateyo.set", function (e, data) { rate = data.rating;})
        .on("rateyo.change", function () { console.log("rateyo.change"); });


});

function startRecognition() {
    window.plugins.speechRecognition.startListening(function (result) {
        // Show results in the console
        console.log(result);
        r.innerText = result[0];
    }, function (err) {
        console.error(err);
    }, {
            language: "en-IN",
            showPopup: true
        });
}
// Verify if recognition is available
window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
    if (!available) {
        console.log("Sorry, not available");
    }
});

function validation() {
    if (isNaN(document.getElementById("resultTime").value) == true || isNaN(document.getElementById("resultAmount").value) == true || isNaN(document.getElementById("resultAwakenings").value) == true)
    {
        alert("Time, Naps and Awake must be numbers");
        return false;
    }

    else if (document.getElementById("resultTime").value == "" || document.getElementById('startSleep').value == "" || document.getElementById('endSleep').value == "")
    {
        alert("Time, Start and End must have a values");
        return false;
    }

    else
        return true;
}

function PostSleep() {

    if (validation()) {
        var firstCheck = true;
        var today = document.getElementById('sleepLogDate').value;
        var Ref1 = firebase.database().ref('sleep/' + uID + '/' + today);
        Ref1.on('value', function (snap) {
            if (snap.val() == null) {
                firstCheck = false;
                var newPostKey = firebase.database().ref().child('sleep').push().key;
                firebase.database().ref('sleep/' + uID + '/' + today + '/' + newPostKey).set({
                    start: document.getElementById('startSleep').value,
                    end: document.getElementById("endSleep").value,
                    falling_asleep: document.getElementById("resultTime").value,
                    naps: document.getElementById("resultAmount").value,
                    awakenings: document.getElementById("resultAwakenings").value,
                    rate: rate
                });
                alert("Saved succesfully");
                window.location.href = "home.html";
            }
            else {
                if (firstCheck == true)
                    alert("SleepLog exists in this date");
            }
        });

    }
}

function info(value) {
    if (value == "start")
        alert("The time when you went to sleep");
    if (value == "end")
        alert("The time when you woke up");
    if (value == "time")
        alert("How much time you needed to fall asleep");
    if (value == "naps")
        alert("How much time you slept during the day");
    if (value == "awake")
        alert("How much time you did not sleep at night");
    if (value == "rate")
        alert("Subjective sleep assessment");
    if (value == "logDate")
        alert("Date when you want add the log");
}