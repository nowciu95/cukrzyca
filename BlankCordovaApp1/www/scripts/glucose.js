﻿
var r;
document.getElementById("but").addEventListener("click", function () { r = document.getElementById('result'); startRecognition(); }, false);
document.getElementById("butHbAlc").addEventListener("click", function () { r = document.getElementById('resultHbAlc'); startRecognition(); }, false);
document.getElementById("butInsulin").addEventListener("click", function () { r = document.getElementById('resultInsulin'); startRecognition(); }, false);
document.getElementById("butType").addEventListener("click", function () { r = document.getElementById('resultType'); startRecognition(); }, false);
document.getElementById("butCategory").addEventListener("click", function () { r = document.getElementById('resultCategory'); startRecognition(); }, false);
document.getElementById("butNote").addEventListener("click", function () { r = document.getElementById('resultNote'); startRecognition(); }, false);


    function startRecognition() {
        window.plugins.speechRecognition.startListening(function (result) {
            // Show results in the console
            console.log(result);
            r.innerText = result[0];
        }, function (err) {
            console.error(err);
        }, {
                language: "en-IN",
                showPopup: true
            });
    }
    // Verify if recognition is available
    window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
        if (!available) {
            console.log("Sorry, not available");
        }
    });