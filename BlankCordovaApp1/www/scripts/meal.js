﻿var r;
var items = new Array();
var productsToSave = new Array();
var numberOfProduct = 0;

$(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var first = true;

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("mealLogDate").value = today;
});

function startRecognitionMeal() {
    r = document.getElementById('searchBar');
        window.plugins.speechRecognition.startListening(function (result) {
            // Show results in the console
            console.log(result);
            r.innerText = result[0];
        }, function (err) {
            console.error(err);
        }, {
                language: "en-IN",
                showPopup: true
            });
    }
    // Verify if recognition is available
    window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
        if (!available) {
            console.log("Sorry, not available");
        }
    });

function getResult(userInput) {
    var nr = 0;
    $('.containerResult').html('');
    var settings = {
        async: true,
        crossDomain: true,
        url: "https://trackapi.nutritionix.com/v2/natural/nutrients",
        method: "POST",
        headers: {
            "x-app-id": "dd913573",
            "x-app-key": "caa87c2d9d1e7bac2098983bb241b24d",
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "219970e5-b92f-b5be-97c4-1a92b1fce685"
        },
        processData: false,
        data: "{\r\n \"query\":\"" + userInput + "\",\r\n \"timezone\": \"US/Eastern\"\r\n}"
    }

    $.ajax(settings).done(function (response) {        

        for (var i = 0; i < response.foods.length; i++) {
            var $newDiv = $('<div class="itemBar">' +
                '<h2>' + response.foods[i].food_name + '<h2>' +
                '<h3>Calories: ' + response.foods[i].nf_calories + '<h3>' +
                '<h3>Fat: ' + response.foods[i].nf_total_fat + '<h3>' +
                '<h3>Carbohydrate: ' + response.foods[i].nf_total_carbohydrate + '<h3>' +
                '<h3>Protein: ' + response.foods[i].nf_protein + '<h3>' +
                '<h3>Sugars: ' + response.foods[i].nf_sugars + '<h3>' +
                '<img width="150" height="150" src="' + response.foods[i].photo.thumb + '">' +
                '</div>');

            $newDiv.attr("id", "div" + nr++);
            $('.containerResult').append($newDiv);
            productsToSave[i] = response.foods[i];
        }       
    });   
    }

function searchValue() {
    var formVal = document.getElementById('searchBar').value;
    getResult(formVal);
}

$('#searchForm').submit(function (e) {
    e.preventDefault();
});

function postMeal() {
    var today = document.getElementById('mealLogDate').value;
    var food_name = "", nf_calories = 0, nf_total_fat = 0, nf_protein = 0, nf_total_carbohydrate = 0, nf_sugars = 0;
    for (var i = 0; i < productsToSave.length; i++)
    {
        food_name = food_name + ";" + productsToSave[i].food_name;
        nf_calories = nf_calories + productsToSave[i].nf_calories;
        nf_total_fat = nf_total_fat + productsToSave[i].nf_total_fat;
        nf_protein = nf_protein + productsToSave[i].nf_protein;
        nf_total_carbohydrate = nf_total_carbohydrate + productsToSave[i].nf_total_carbohydrate;
        nf_sugars = nf_sugars + productsToSave[i].nf_sugars;
    }
    var newPostKey = firebase.database().ref().child('food').push().key;
    firebase.database().ref('food/' + uID + '/' + today + '/' + newPostKey).set({
        product: food_name,
        calories: nf_calories,
        fat: nf_total_fat,
        proteins: nf_protein,
        carbohydrates: nf_total_carbohydrate,
        sugars: nf_sugars
    });
    alert("Saved succesfully");
    window.location.href = "home.html";
}

function info(value)
{
    if (value == "meal")
        alert("What you ate e.g: I ate 100g chicken with 100g rice");
    if (value == "logDate")
        alert("Date when you want add the log");
}