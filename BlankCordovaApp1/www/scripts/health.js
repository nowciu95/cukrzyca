﻿var r;

$(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var first = true;

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("healthLogDate").value = today;
});

document.getElementById("butTemp").addEventListener("click", function () { r = document.getElementById('resultTemp'); startRecognition(); }, false);
document.getElementById("butWeight").addEventListener("click", function () { r = document.getElementById('resultWeight'); startRecognition(); }, false);
document.getElementById("butLDL").addEventListener("click", function () { r = document.getElementById('resultLDL'); startRecognition(); }, false);
document.getElementById("butHDL").addEventListener("click", function () { r = document.getElementById('resultHDL'); startRecognition(); }, false);
document.getElementById("butSystolic").addEventListener("click", function () { r = document.getElementById('resultSystolic'); startRecognition(); }, false);
document.getElementById("butDiastolic").addEventListener("click", function () { r = document.getElementById('resultDiastolic'); startRecognition(); }, false);
document.getElementById("butPulse").addEventListener("click", function () { r = document.getElementById('resultPulse'); startRecognition(); }, false);


function startRecognition() {
    window.plugins.speechRecognition.startListening(function (result) {
        // Show results in the console
        console.log(result);
        r.innerText = result[0];
    }, function (err) {
        console.error(err);
    }, {
            language: "en-IN",
            showPopup: true
        });
}
// Verify if recognition is available
window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
    if (!available) {
        console.log("Sorry, not available");
    }
});

function validation() {
    if (isNaN(document.getElementById("resultTemp").value) == true || isNaN(document.getElementById("resultWeight").value) == true || isNaN(document.getElementById("resultLDL").value) == true || 
        isNaN(document.getElementById("resultHDL").value) == true || isNaN(document.getElementById("resultSystolic").value) == true || isNaN(document.getElementById("resultDiastolic").value) == true ||
        isNaN(document.getElementById("resultPulse").value) == true) {
        alert("Terms of data must be numbers");
        return false;
    }

    else if (document.getElementById("resultTemp").value == "" || document.getElementById("resultWeight").value == "" || document.getElementById("resultLDL").value == "" ||
        document.getElementById("resultHDL").value == "" || document.getElementById("resultSystolic").value == "" || document.getElementById("resultDiastolic").value == "" ||
        document.getElementById("resultPulse").value == "") {
        alert("Terms of data must have values");
        return false;
    }

    else
        return true;
}

function PostHealth() {

    if (validation()) {
        var firstCheck = true;
        var today = document.getElementById('healthLogDate').value;
        var Ref1 = firebase.database().ref('health/' + uID + '/' + today);
        Ref1.on('value', function (snap) {
            if (snap.val() == null) {
                firstCheck = false;
                var newPostKey = firebase.database().ref().child('health').push().key;
                firebase.database().ref('health/' + uID + '/' + today + '/' + newPostKey).set({
                    temperature: document.getElementById('resultTemp').value,
                    weight: document.getElementById("resultWeight").value,
                    ldl: document.getElementById("resultLDL").value,
                    hdl: document.getElementById("resultHDL").value,
                    systolic: document.getElementById("resultSystolic").value,
                    diastolic: document.getElementById("resultDiastolic").value,
                    pulse: document.getElementById("resultPulse").value
                });
                alert("Saved succesfully");
                window.location.href = "home.html";
            }
            else {
                if (firstCheck == true)
                    alert("SleepLog exists in this date");
            }
        });

    }
}

function info(value) {
    if (value == "temp")
        alert("Body temperature");
    if (value == "weight")
        alert("Body weight");
    if (value == "LDL")
        alert("Cholesterol LDL (bad)");
    if (value == "HDL")
        alert("Cholesterol HDL (good)");
    if (value == "systolic")
        alert("Systolic blood pressure");
    if (value == "diastolic")
        alert("Diastolic blood pressure");
    if (value == "pulse")
        alert("Pulse");
    if (value == "logDate")
        alert("Date when you want add the log");
}