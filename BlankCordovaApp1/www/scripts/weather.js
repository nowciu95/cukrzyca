﻿var OpenWeatherAppKey = "ca236dd7edd6b85f65b3c410c8774676";
var temp, pres, humi;
$('#get-weather-btn').click(getWeatherWithZipCode);
$('#get-weather-btn2').click(getWeatherWithGeoLocation);

function getWeatherWithZipCode() {
 var zipcode = $('#zip-code-input').val();
 var queryString =
     'http://api.openweathermap.org/data/2.5/weather?zip='
     + zipcode + ',us&appid=' + OpenWeatherAppKey + '&units=imperial';
 $.getJSON(queryString, function (results) {
     showWeatherData(results);
     }).fail(function (jqXHR) {
         $('#error-msg').show();
         $('#error-msg').text("Error retrieving data. " + jqXHR.statusText);
     });
     return false;
}

function getWeatherWithGeoLocation() {
    //Call the Cordova Geolocation API
    navigator.geolocation.getCurrentPosition(onGetLocationSuccess, onGetLocationError,
        { enableHighAccuracy: true });
    $('#error-msg').show();
    $('#error-msg').text('Determining your current location ...');
    $('#get-weather-btn').prop('disabled', true);
}

function onGetLocationSuccess(position) {
    //Retrieve the location information from the position object
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    console.log(position);
    var queryString = 'http://api.openweathermap.org/data/2.5/weather?lat='
        + latitude + '&lon=' + longitude + '&appid=' + OpenWeatherAppKey + '&units=imperial';

    $('#get-weather-btn').prop('disabled', false);

    $.getJSON(queryString, function (results) {
        showWeatherData(results);
    }).fail(function (jqXHR) {
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. " + jqXHR.statusText);
    });
}

function onGetLocationError(error) {
    $('#error-msg').text('Error getting location');
    $('#get-weather-btn').prop('disabled', false);
}

function showWeatherData(results) {

 if (results.weather.length) {
     $('#error-msg').hide();
     $('#weather-data').show();

     $('#titleTable').text(results.name);
     $('#temperature').text(results.main.temp);
     $('#wind').text(results.wind.speed);
     $('#humidity').text(results.main.humidity);
     $('#pressure').text(results.main.pressure);
     $('#visibility').text(results.weather[0].main);

     var sunriseDate = new Date(results.sys.sunrise * 1000);
     $('#sunrise').text(sunriseDate.toLocaleTimeString());

     var sunsetDate = new Date(results.sys.sunset * 1000);
     $('#sunset').text(sunsetDate.toLocaleTimeString());
     temp = results.main.temp;
     pres = results.main.pressure;
     humi = results.main.humidity;

 } else {
     $('#weather-data').hide();
     $('#error-msg').show();
     $('#error-msg').text("Error retrieving data. ");
 }
}

function PostWeather() {
    if (temp != null && humi != null && pres != null) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        var firstCheck = true;

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;

        var Ref1 = firebase.database().ref('weather/' + uID + '/' + today);
        Ref1.on('value', function (snap) {
            if (snap.val() == null) {
                firstCheck = false;
                var newPostKey = firebase.database().ref().child('weather').push().key;
                firebase.database().ref('weather/' + uID + '/' + today + '/' + newPostKey).set({
                    temperature: temp,
                    humidity: humi,
                    pressure: pres
                });
                alert("Saved succesfully");
                window.location.href = "home.html";
            }
            else {
                if (firstCheck == true)
                    alert("WeatherLog exists in this date");
            }
        });
    }
}

function info(value)
{
    if (value == "zip")
        alert("The zip-code of the place you want to find a weather");
}