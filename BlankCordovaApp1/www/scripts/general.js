﻿var r,rateMood = 0, rateStress= 0;

$(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var first = true;

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("generalLogDate").value = today;
});

document.getElementById("butWork").addEventListener("click", function () { r = document.getElementById('resultWork'); startRecognition(); }, false);
document.getElementById("butCigarettes").addEventListener("click", function () { r = document.getElementById('resultCigarettes'); startRecognition(); }, false);

$(function () {

    $("#mood").on("rateyo.init", function () { console.log("rateyo.init"); });

    $("#mood").rateYo({

        numStars: 5,
        starWidth: "25px",
        spacing: "5px",
        fullStar: true,

        onInit: function () {

            console.log("On Init");
        },
        onSet: function () {

            console.log("On Set");
        }
    }).on("rateyo.set", function (e, data) { rateMood = data.rating;})
        .on("rateyo.change", function () { console.log("rateyo.change"); });

    $("#stress").on("rateyo.init", function () { console.log("rateyo.init"); });

    $("#stress").rateYo({

        numStars: 5,
        starWidth: "25px",
        spacing: "5px",
        fullStar: true,

        onInit: function () {

            console.log("On Init");
        },
        onSet: function () {

            console.log("On Set");
        }
    }).on("rateyo.set", function (e, data) { rateStress = data.rating; })
        .on("rateyo.change", function () { console.log("rateyo.change"); });
});



function startRecognition() {
    window.plugins.speechRecognition.startListening(function (result) {
        // Show results in the console
        console.log(result);
        r.innerText = result[0];
    }, function (err) {
        console.error(err);
    }, {
            language: "en-IN",
            showPopup: true
        });
}
// Verify if recognition is available
window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
    if (!available) {
        console.log("Sorry, not available");
    }
});

function validation() {
    if (isNaN(document.getElementById("resultWork").value) == true || isNaN(document.getElementById("resultCigarettes").value) == true)
    {
        alert("Work and Cigarettes must be numbers");
        return false;
    }

    else if (document.getElementById("resultWork").value == "" || document.getElementById('resultCigarettes').value == "")
    {
        alert("Work and Cigarettes must have a values");
        return false;
    }

    else
        return true;
}

function PostGeneral() {

    if (validation()) {
        var firstCheck = true;
        var alco, cof;

        if (document.getElementById("alcoholCheckbox").checked == true)
        {
            alco = "yes";
        }
        else
        {
            alco = "no";
        }

        if (document.getElementById("cofeineCheckbox").checked == true) {
            cof = "yes";
        }
        else {
            cof = "no";
        }

        var today = document.getElementById('generalLogDate').value;
        var Ref1 = firebase.database().ref('general/' + uID + '/' + today);
        Ref1.on('value', function (snap) {
            if (snap.val() == null) {
                firstCheck = false;
                var newPostKey = firebase.database().ref().child('general').push().key;
                firebase.database().ref('general/' + uID + '/' + today + '/' + newPostKey).set({
                    work: document.getElementById("resultWork").value,
                    cigarettes: document.getElementById("resultCigarettes").value,
                    alcohol: alco,
                    caffeine: cof,
                    general_mood: rateMood,
                    stress: rateStress
                });
                alert("Saved succesfully");
                window.location.href = "home.html";
            }
            else {
                if (firstCheck == true)
                    alert("GeneralLog exists in this date");
            }
        });

    }
}

function info(value) {
    if (value == "work")
        alert("How much time did you work");
    if (value == "cigarettes")
        alert("If you smoked a cigarettes");
    if (value == "alcohol")
        alert("If you were drinking alcohol");
    if (value == "cofeine")
        alert("If you were drinking coffee");
    if (value == "generalmood")
        alert("Rate your mood");
    if (value == "stress")
        alert("Rate your stress");
    if (value == "logDate")
        alert("Date when you want add the log");
}