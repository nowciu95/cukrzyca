﻿var i = 0, k = 0, d = 0, l = 0, c = 0;
var dates = new Array(7);
var datesHelp = new Array(7);
var datesChart = new Array(7);
var keys = new Array(7);
var dateChanged = 0;
var fearst = true;
var pres = [];
var deleteHelp;

function getKeys(colect) {
    var today = new Date();

    for (var j = 0; j < 7; j++) {
        var priorDate = new Date().setDate(today.getDate() - (6 - j));
        var newDate = new Date(priorDate);
        datesHelp[j] = newDate;
    }
    for (var i = 0; i < 7; i++) {
        var t;
        var dd = datesHelp[i].getDate();
        var mm = datesHelp[i].getMonth() + 1; //January is 0!
        var yyyy = datesHelp[i].getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        t = yyyy + '-' + mm + '-' + dd;
        dates[i] = t;

        var Ref = firebase.database().ref(colect + uID + '/' + t);
        Ref.on('value', function (snapshot) {
            flag = true;
            snapshot.forEach(p);
            function p(userSnapshot) {

                var Ref1 = firebase.database().ref(colect + uID + '/' + dates[d] + '/' + userSnapshot.key);
                Ref1.on('value', function (snap) {
                    if (colect == "sleep/") {
                        var parts = dates[d].split('-');
                        var a = new Date(parts[0] - 1, parts[1], parts[2]);
                        a = (a.getFullYear() + 1) + '/' + (((a.getMonth()) < 10) ? '0' : '') + (a.getMonth()) + '/' + ((a.getDate() < 10) ? '0' : '') + a.getDate();
                        pres.push({ id: userSnapshot.key, date: a, naps: snap.val().naps, falling_asleep: snap.val().falling_asleep, rate: snap.val().rate, awakenings: snap.val().awakenings, start: snap.val().start, end: snap.val().end });                        
                    }
                    else if (colect == "food/") {                   
                            var parts = dates[d].split('-');
                            var a = new Date(parts[0] - 1, parts[1], parts[2]);
                            a = (a.getFullYear() + 1) + '/' + (((a.getMonth()) < 10) ? '0' : '') + (a.getMonth()) + '/' + ((a.getDate() < 10) ? '0' : '') + a.getDate();                       
                            pres.push({ id: userSnapshot.key, date: a, calories: snap.val().calories, fat: snap.val().fat, protein: snap.val().proteins, carbohydrates: snap.val().carbohydrates, sugars: snap.val().sugars});                        
                    }
                    else if (colect == "weather/") {
                        var parts = dates[d].split('-');
                        var a = new Date(parts[0] - 1, parts[1], parts[2]);
                        a = (a.getFullYear() + 1) + '/' + (((a.getMonth()) < 10) ? '0' : '') + (a.getMonth()) + '/' + ((a.getDate() < 10) ? '0' : '') + a.getDate();
                        pres.push({ id: userSnapshot.key, date: a, temp: snap.val().temperature, humi: snap.val().humidity, pressure: snap.val().pressure}); 
                    }
                    else if (colect == "health/") {
                        var parts = dates[d].split('-');
                        var a = new Date(parts[0] - 1, parts[1], parts[2]);
                        a = (a.getFullYear() + 1) + '/' + (((a.getMonth()) < 10) ? '0' : '') + (a.getMonth()) + '/' + ((a.getDate() < 10) ? '0' : '') + a.getDate();
                        pres.push({ id: userSnapshot.key, date: a, temp: snap.val().temperature, weight: snap.val().weight, LDL: snap.val().ldl, HDL: snap.val().hdl, systolic: snap.val().systolic, diastolic: snap.val().diastolic, pulse: snap.val().pulse}); 
                    }
                    else if (colect == "sport/") {
                        var parts = dates[d].split('-');
                        var a = new Date(parts[0] - 1, parts[1], parts[2]);
                        a = (a.getFullYear() + 1) + '/' + (((a.getMonth()) < 10) ? '0' : '') + (a.getMonth()) + '/' + ((a.getDate() < 10) ? '0' : '') + a.getDate();
                        pres.push({ id: userSnapshot.key, date: a, calories: snap.val().calories, sport: snap.val().sport, duration: snap.val().duration });  
                    }
                    else if (colect == "general/") {
                        var parts = dates[d].split('-');
                        var a = new Date(parts[0] - 1, parts[1], parts[2]);
                        a = (a.getFullYear() + 1) + '/' + (((a.getMonth()) < 10) ? '0' : '') + (a.getMonth()) + '/' + ((a.getDate() < 10) ? '0' : '') + a.getDate();
                        pres.push({ id: userSnapshot.key, date: a, alcohol: snap.val().alcohol, cigarettes: snap.val().cigarettes, cofeine: snap.val().caffeine, mood: snap.val().general_mood, stress: snap.val().stress, work: snap.val().work  });
                    }
                    l++;
                });
            }
            d++;
            if (d == 7)
                dataTable(colect);
        });
    }
}

function dataTable(colect) {
    var dataSet = [];
    var dt;
    if (colect == "sleep/") {
        $(document).ready(function () {
          $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
          dt =  $('#example').DataTable({
              data: dataSet,
              paging: false,
              columns: [
                    { title: "Id" },
                    { title: "Date" },
                    { title: "Awake" },
                    { title: "Naps" },
                    { title: "Time" },
                    { title: "Rate" },
                    { title: "Start" },
                    { title: "End" }
                ]             
            });
            for (var i = 0; i < pres.length; i++)
            {
                dt.row.add([pres[i].id, pres[i].date, pres[i].awakenings, pres[i].naps, pres[i].falling_asleep, pres[i].rate, pres[i].start, pres[i].end]).draw();
          }
             i = 0, k = 0, d = 0, l = 0, c = 0;
             dates = new Array(7);
             datesHelp = new Array(7);
             datesChart = new Array(7);
             keys = new Array(7);
             dateChanged = 0;
             fearst = true;
             pres = [];
             deleteHelp = "sleep/";
             var column = dt.column($(this).attr('Id'));
             column.visible(!column.visible());

             $('#example tbody').on('click', 'tr', function () {
                 if ($(this).hasClass('selected')) {
                     $(this).removeClass('selected');
                 }
                 else {
                     dt.$('tr.selected').removeClass('selected');
                     $(this).addClass('selected');
                 }
             });

             $('#button').click(function () {
                 var dat = dt.row('.selected').data()[1].replace(/\//g, "-");
                 var pomId = dt.row('.selected').data()[0];
                 deleteHelp = deleteHelp + uID + "/" + dat + "/" + pomId;
                 firebase.database().ref(deleteHelp).remove();
                 dt.row('.selected').remove().draw(false);
             });
        });
      
    }

    if (colect == "food/") {
        $(document).ready(function () {
            $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
            dt = $('#example').DataTable({
                data: dataSet,
                paging: false,
                columns: [
                { title: "Id" },
                { title: "Date" },
                { title: "Calories" },
                { title: "Fat" },
                { title: "Protein" },
                { title: "Carbohydrates" },
                { title: "Sugars" }
            ]
            });
            for (var i = 0; i < pres.length; i++) {
                dt.row.add([pres[i].id, pres[i].date, pres[i].calories, pres[i].fat, pres[i].protein, pres[i].carbohydrates, pres[i].sugars]).draw();
            }
            i = 0, k = 0, d = 0, l = 0, c = 0;
            dates = new Array(7);
            datesHelp = new Array(7);
            datesChart = new Array(7);
            keys = new Array(7);
            dateChanged = 0;
            fearst = true;
            pres = [];
            deleteHelp = "food/";
            var column = dt.column($(this).attr('Id'));
            column.visible(!column.visible());

            $('#example tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    dt.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            $('#button').click(function () {
                var dat = dt.row('.selected').data()[1].replace(/\//g, "-");
                var pomId = dt.row('.selected').data()[0];
                deleteHelp = deleteHelp + uID + "/" + dat + "/" + pomId;
                firebase.database().ref(deleteHelp).remove();
                dt.row('.selected').remove().draw(false);
            });
        });

    }

    if (colect == "weather/") {
        var dataSet = [];
        var dt;
        $(document).ready(function () {
            $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
            dt = $('#example').DataTable({
                data: dataSet,
                paging: false,
                columns: [
                    { title: "Id" },
                    { title: "Date" },
                    { title: "Temperature" },
                    { title: "Humidity" },
                    { title: "Pressure" }
                ]
            });
            for (var i = 0; i < pres.length; i++) {
                dt.row.add([pres[i].id, pres[i].date, pres[i].temp, pres[i].humi, pres[i].pressure]).draw();
            }
            i = 0, k = 0, d = 0, l = 0, c = 0;
            dates = new Array(7);
            datesHelp = new Array(7);
            datesChart = new Array(7);
            keys = new Array(7);
            dateChanged = 0;
            fearst = true;
            pres = [];
            deleteHelp = "weather/";
            var column = dt.column($(this).attr('Id'));
            column.visible(!column.visible());

            $('#example tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    dt.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            $('#button').click(function () {
                var dat = dt.row('.selected').data()[1].replace(/\//g, "-");
                var pomId = dt.row('.selected').data()[0];
                deleteHelp = deleteHelp + uID + "/" + dat + "/" + pomId;
                firebase.database().ref(deleteHelp).remove();
                dt.row('.selected').remove().draw(false);
            });
        });

    }

    if (colect == "health/") {
        var dataSet = [];
        var dt;
        $(document).ready(function () {
            $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
            dt = $('#example').DataTable({
                data: dataSet,
                paging: false,
                columns: [
                    { title: "Id" },
                    { title: "Date" },
                    { title: "Temperature" },
                    { title: "Weight" },
                    { title: "LDL" },
                    { title: "HDL" },
                    { title: "Systolic" },
                    { title: "Diastolic" },
                    { title: "Pulse" }
                ]
            });
            for (var i = 0; i < pres.length; i++) {
                dt.row.add([pres[i].id, pres[i].date, pres[i].temp, pres[i].weight, pres[i].LDL, pres[i].HDL, pres[i].systolic, pres[i].diastolic, pres[i].pulse]).draw();
            }
            i = 0, k = 0, d = 0, l = 0, c = 0;
            dates = new Array(7);
            datesHelp = new Array(7);
            datesChart = new Array(7);
            keys = new Array(7);
            dateChanged = 0;
            fearst = true;
            pres = [];
            deleteHelp = "health/";
            var column = dt.column($(this).attr('Id'));
            column.visible(!column.visible());

            $('#example tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    dt.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            $('#button').click(function () {
                var dat = dt.row('.selected').data()[1].replace(/\//g, "-");
                var pomId = dt.row('.selected').data()[0];
                deleteHelp = deleteHelp + uID + "/" + dat + "/" + pomId;
                firebase.database().ref(deleteHelp).remove();
                dt.row('.selected').remove().draw(false);
            });
        });

    }

    if (colect == "sport/") {
        var dataSet = [];
        var dt;
        $(document).ready(function () {
            $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
            dt = $('#example').DataTable({
                data: dataSet,
                paging: false,
                columns: [
                    { title: "Id" },
                    { title: "Date" },
                    { title: "Calories" },
                    { title: "Sport" },
                    { title: "Time" }
                ]
            });
            for (var i = 0; i < pres.length; i++) {
                dt.row.add([pres[i].id, pres[i].date, pres[i].calories, pres[i].sport, pres[i].duration]).draw();
            }
            i = 0, k = 0, d = 0, l = 0, c = 0;
            dates = new Array(7);
            datesHelp = new Array(7);
            datesChart = new Array(7);
            keys = new Array(7);
            dateChanged = 0;
            fearst = true;
            pres = [];
            deleteHelp = "sport/";
            var column = dt.column($(this).attr('Id'));
            column.visible(!column.visible());

            $('#example tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    dt.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            $('#button').click(function () {
                var dat = dt.row('.selected').data()[1].replace(/\//g, "-");
                var pomId = dt.row('.selected').data()[0];
                deleteHelp = deleteHelp + uID + "/" + dat + "/" + pomId;
                firebase.database().ref(deleteHelp).remove();
                dt.row('.selected').remove().draw(false);
            });
        });

    }

    if (colect == "general/") {
        var dataSet = [];
        var dt;
        $(document).ready(function () {
            $('#demo').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"></table>');
            dt = $('#example').DataTable({
                data: dataSet,
                paging: false,
                columns: [
                    { title: "Id" },
                    { title: "Date" },
                    { title: "Alcohol" },
                    { title: "Cigarettes" },
                    { title: "Caffeine" },
                    { title: "Mood" },
                    { title: "Stress" },
                    { title: "Work" }
                ]
            });
            for (var i = 0; i < pres.length; i++) {
                dt.row.add([pres[i].id, pres[i].date, pres[i].alcohol, pres[i].cigarettes, pres[i].cofeine, pres[i].mood, pres[i].stress, pres[i].work]).draw();
            }
            i = 0, k = 0, d = 0, l = 0, c = 0;
            dates = new Array(7);
            datesHelp = new Array(7);
            datesChart = new Array(7);
            keys = new Array(7);
            dateChanged = 0;
            fearst = true;
            pres = [];
            deleteHelp = "general/";
            var column = dt.column($(this).attr('Id'));
            column.visible(!column.visible());

            $('#example tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    dt.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            $('#button').click(function () {
                var dat = dt.row('.selected').data()[1].replace(/\//g, "-");
                var pomId = dt.row('.selected').data()[0];
                deleteHelp = deleteHelp + uID + "/" + dat + "/" + pomId;
                firebase.database().ref(deleteHelp).remove();
                dt.row('.selected').remove().draw(false);
            });
        });

    }
}