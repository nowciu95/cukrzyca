﻿var r;

document.getElementById("butAge").addEventListener("click", function () { r = document.getElementById('resultAge'); startRecognition(); }, false);
document.getElementById("butWeight").addEventListener("click", function () { r = document.getElementById('resultWeight'); startRecognition(); }, false);
document.getElementById("butHeight").addEventListener("click", function () { r = document.getElementById('resultHeight'); startRecognition(); }, false);


function startRecognition() {
    window.plugins.speechRecognition.startListening(function (result) {
        // Show results in the console
        console.log(result);
        r.innerText = result[0];
    }, function (err) {
        console.error(err);
    }, {
            language: "en-IN",
            showPopup: true
        });
}
// Verify if recognition is available
window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
    if (!available) {
        console.log("Sorry, not available");
    }
});

function validation() {
    if (isNaN(document.getElementById("resultAge").value) == true || isNaN(document.getElementById("resultWeight").value) == true || isNaN(document.getElementById("resultHeight").value) == true) {
        alert("Terms of data must be numbers");
        return false;
    }

    else if (document.getElementById("resultAge").value == "" || document.getElementById("resultWeight").value == "" || document.getElementById("resultHeight").value == "" ) {
        alert("Terms of data must have values");
        return false;
    }

    else
        return true;
}

function PostSettings() {

    if (validation()) {
        var Ref1 = firebase.database().ref('settings/' + uID);
        Ref1.on('value', function (snap) {
            
            firebase.database().ref('settings/' + uID).set({
                height: document.getElementById('resultHeight').value,
                weight: document.getElementById('resultWeight').value,
                age: document.getElementById('resultAge').value,
                gander: document.getElementById('resultSex').value
                });
                alert("Saved succesfully");
                window.location.href = "home.html";          
        });
    }
}

function info(value) {
    if (value == "age")
        alert("Your age");
    if (value == "weight")
        alert("Body weight");
    if (value == "height")
        alert("Body height");
    if (value == "sex")
        alert("Are you male or famale");
}