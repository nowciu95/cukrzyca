﻿var awakeningsGet = new Array(7);
var falling_asleepGet = new Array(7);
document.getElementById("backButton").style.visibility = "hidden";
var napsGet = new Array(7);
var rateGet = new Array(7);
var dateGet = [];
var i = 0, k = 0, d=0, l=0, c=0;
var dates = new Array(7);
var datesHelp = new Array(7);
var datesChart = new Array(7);
var keys = new Array(7);
var dateChanged = 0;
var fearst = true;
var colection;
var caloriesGet = [0, 0, 0, 0, 0, 0, 0];
var fatGet = [0, 0, 0, 0, 0, 0, 0];
var carbohydratesGet = [0, 0, 0, 0, 0, 0, 0];
var proteinGet = [0, 0, 0, 0, 0, 0, 0];
var sugarsGet = [0, 0, 0, 0, 0, 0, 0];
var pres = [], temp = [], humi = [];
var temperature = [], weight = [], LDL = [], HDL = [], systolic = [], diastolic = [], pulse = [], cigarettes = [], mood = [], stress = [], work = [];
var calo = [0, 0, 0, 0, 0, 0, 0];


function getKeys(colect)
{
    document.getElementById("backButton").style.visibility = "visible";
    var today = new Date();
   
    for (var j = 0; j < 7; j++)
    {
        var priorDate = new Date().setDate(today.getDate() - (6-j));
        var newDate = new Date(priorDate);
        datesHelp[j] = newDate;
    }
    for (var i = 0; i < 7; i++) {
        var t;
        var dd = datesHelp[i].getDate();
        var mm = datesHelp[i].getMonth() + 1; //January is 0!
        var yyyy = datesHelp[i].getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        t = yyyy + '-' + mm + '-' + dd;
        dates[i] = t;

        var Ref = firebase.database().ref(colect + uID + '/' + t);
        Ref.on('value', function (snapshot) {
            flag = true;
            snapshot.forEach(p);
            function p(userSnapshot) {

                var Ref1 = firebase.database().ref(colect + uID + '/' + dates[d] + '/' + userSnapshot.key);
                Ref1.on('value', function (snap) {
                    if (colect == "sleep/") {
                        napsGet[l] = snap.val().naps;
                        falling_asleepGet[l] = snap.val().falling_asleep;
                        rateGet[l] = snap.val().rate;
                        awakeningsGet[l] = snap.val().awakenings;
                        var parts = dates[d].split('-');
                        dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                    }
                    else if (colect == "food/")
                    {
                        if (fearst == true)
                        {
                            fearst = false;
                            dateChanged = d;
                            var parts = dates[d].split('-');
                            dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                        }
                        else if (d > dateChanged)
                        {
                            dateChanged = d;
                            c++;
                            var parts = dates[d].split('-');
                            dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                        }
                    
                        caloriesGet[c] = caloriesGet[c] + parseInt(snap.val().calories);
                        fatGet[c] = fatGet[c] + parseInt(snap.val().fat);
                        proteinGet[c] = proteinGet[c] + parseInt(snap.val().proteins);
                        carbohydratesGet[c] = carbohydratesGet[c] + parseInt(snap.val().carbohydrates);
                        sugarsGet[c] = sugarsGet[c] + parseInt(snap.val().sugars);

                    }
                    else if (colect == "weather/")
                    {
                        temp.push(snap.val().temperature);
                        humi.push(snap.val().humidity);
                        pres.push(snap.val().pressure);
                        var parts = dates[d].split('-');
                        dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                    }
                    else if (colect == "health/") {
                        temperature.push(snap.val().temperature);
                        weight.push(snap.val().weight);
                        LDL.push(snap.val().ldl);
                        HDL.push(snap.val().hdl);
                        systolic.push(snap.val().systolic);
                        diastolic.push(snap.val().diastolic);
                        pulse.push(snap.val().pulse);
                        var parts = dates[d].split('-');
                        dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                    }
                    else if (colect == "general/") {
                        cigarettes.push(snap.val().cigarettes);
                        mood.push(snap.val().general_mood);
                        stress.push(snap.val().stress);
                        work.push(snap.val().work);
                        var parts = dates[d].split('-');
                        dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                    }
                    else if (colect == "sport/") {
                        if (fearst == true) {
                            fearst = false;
                            dateChanged = d;
                            var parts = dates[d].split('-');
                            dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                        }
                        else if (d > dateChanged) {
                            dateChanged = d;
                            c++;
                            var parts = dates[d].split('-');
                            dateGet.push(new Date(parts[0] - 1, parts[1], parts[2]));
                        }

                        calo[c] = calo[c] + parseInt(snap.val().calories);
                    }
                    l++;
                });              
            }
            d++;
            if (d == 7)
                loadCharts(colect);
        });
    }
       
   
}

function loadCharts(colect) {

    if (colect == "sleep/") {
        var dps = [];
        var dps1 = [];
        var dps2 = [];
        var dps3 = [];
        for (var i = 0; i < dateGet.length; i++) {
            dps.push({ "x": dateGet[i], "y": parseInt(awakeningsGet[i]) });
            dps1.push({ "x": dateGet[i], "y": parseInt(rateGet[i]) });
            dps2.push({ "x": dateGet[i], "y": parseInt(falling_asleepGet[i]) });
            dps3.push({ "x": dateGet[i], "y": parseInt(napsGet[i]) });
        }
        var options = {
            animationEnabled: true,
            title: {
                text: "Sleep"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                itemclick: toogleDataSeries
            },
            data: [
                {
                    showInLegend: true,
                    name: "Awakenings",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps
                },
                {
                    showInLegend: true,
                    name: "Rate",
                    type: "line",
                    color: "#ff0000",
                    lineThickness: 4,
                    dataPoints: dps1
                },
                {
                    showInLegend: true,
                    name: "Falling asleep",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps2
                },
                {
                    showInLegend: true,
                    name: "Naps",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps3
                }
            ]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
    else if (colect == "food/")
    {
        var dps = [];
        var dps1 = [];
        var dps2 = [];
        var dps3 = [];
        var dps4 = [];
        for (var i = 0; i < dateGet.length; i++) {
            dps.push({ "x": dateGet[i], "y": caloriesGet[i] });
            dps1.push({ "x": dateGet[i], "y": fatGet[i] });
            dps2.push({ "x": dateGet[i], "y": proteinGet[i] });
            dps3.push({ "x": dateGet[i], "y": carbohydratesGet[i] });
            dps4.push({ "x": dateGet[i], "y": sugarsGet[i] });

        }
        var options = {
            animationEnabled: true,
            title: {
                text: "Food"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                itemclick: toogleDataSeries
            },
            data: [
                {
                    showInLegend: true,
                    name: "Calories",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps
                },
                {
                    showInLegend: true,
                    name: "Fat",
                    type: "line",
                    dataPoints: dps1
                },
                {
                    showInLegend: true,
                    name: "Protein",
                    type: "line",
                    dataPoints: dps2
                },
                {
                    showInLegend: true,
                    name: "Carbohydrates",
                    type: "line",
                    dataPoints: dps3
                },
                {
                    showInLegend: true,
                    name: "Sugars",
                    type: "line",
                    dataPoints: dps4
                }
            ]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
    else if (colect == "weather/") {
        var dps = [];
        var dps1 = [];
        var dps2 = [];
        for (var i = 0; i < dateGet.length; i++) {
            dps.push({ "x": dateGet[i], "y": parseInt(temp[i]) });
            dps1.push({ "x": dateGet[i], "y": parseInt(humi[i]) });
            dps2.push({ "x": dateGet[i], "y": parseInt(pres[i]) });
        }
        var options = {
            animationEnabled: true,
            title: {
                text: "Weather"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                itemclick: toogleDataSeries
            },
            data: [
                {
                    showInLegend: true,
                    name: "Temperature",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps
                },
                {
                    showInLegend: true,
                    name: "Humidity(%)",
                    type: "line",
                    dataPoints: dps1
                },
                {
                    showInLegend: true,
                    name: "Pressure(hPa)",
                    type: "line",
                    dataPoints: dps2
                }
            ]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
    else if (colect == "health/") {
        var dps = [];
        var dps1 = [];
        var dps2 = [];
        var dps3 = [];
        var dps4 = [];
        var dps5 = [];
        var dps6 = [];
        for (var i = 0; i < dateGet.length; i++) {
            dps.push({ "x": dateGet[i], "y": parseInt(temperature[i]) });
            dps1.push({ "x": dateGet[i], "y": parseInt(weight[i]) });
            dps2.push({ "x": dateGet[i], "y": parseInt(LDL[i]) });
            dps3.push({ "x": dateGet[i], "y": parseInt(HDL[i]) });
            dps4.push({ "x": dateGet[i], "y": parseInt(systolic[i]) });
            dps5.push({ "x": dateGet[i], "y": parseInt(diastolic[i]) });
            dps6.push({ "x": dateGet[i], "y": parseInt(pulse[i]) });

        }
        var options = {
            animationEnabled: true,
            title: {
                text: "Health"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                itemclick: toogleDataSeries
            },
            data: [
                {
                    showInLegend: true,
                    name: "Temperature",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps
                },
                {
                    showInLegend: true,
                    name: "Weight",
                    type: "line",
                    dataPoints: dps1
                },
                {
                    showInLegend: true,
                    name: "LDL",
                    type: "line",
                    dataPoints: dps2
                },
                {
                    showInLegend: true,
                    name: "HDL",
                    type: "line",
                    dataPoints: dps3
                },
                {
                    showInLegend: true,
                    name: "Systolic",
                    type: "line",
                    dataPoints: dps4
                },
                {
                    showInLegend: true,
                    name: "Diastolic",
                    type: "line",
                    dataPoints: dps5
                },
                {
                    showInLegend: true,
                    name: "Pulse",
                    type: "line",
                    dataPoints: dps6
                }
            ]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
    else if (colect == "general/") {
        var dps = [];
        var dps1 = [];
        var dps2 = [];
        var dps3 = [];
        for (var i = 0; i < dateGet.length; i++) {
            dps.push({ "x": dateGet[i], "y": parseInt(cigarettes[i]) });
            dps1.push({ "x": dateGet[i], "y": parseInt(mood[i]) });
            dps2.push({ "x": dateGet[i], "y": parseInt(stress[i]) });
            dps3.push({ "x": dateGet[i], "y": parseInt(work[i]) });
        }
        var options = {
            animationEnabled: true,
            title: {
                text: "General"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                itemclick: toogleDataSeries
            },
            data: [
                {
                    showInLegend: true,
                    name: "Cigarettes",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps
                },
                {
                    showInLegend: true,
                    name: "Mood",
                    type: "line",
                    dataPoints: dps1
                },
                {
                    showInLegend: true,
                    name: "Stress",
                    type: "line",
                    dataPoints: dps2
                },
                {
                    showInLegend: true,
                    name: "Work",
                    type: "line",
                    dataPoints: dps3
                }
            ]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
    else if (colect == "sport/") {
        var dps = [];
        for (var i = 0; i < dateGet.length; i++) {
            dps.push({ "x": dateGet[i], "y": calo[i] });
        }
        var options = {
            animationEnabled: true,
            title: {
                text: "Sport"
            },
            toolTip: {
                shared: true
            },
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "left",
                itemclick: toogleDataSeries
            },
            data: [
                {
                    showInLegend: true,
                    name: "Calories",
                    type: "line", //change it to line, area, bar, pie, etc
                    dataPoints: dps
                }
            ]
        };
        $("#chartContainer").CanvasJSChart(options);
    }
    function toogleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
}

function back() {
    window.location.href = "reports.html";
}