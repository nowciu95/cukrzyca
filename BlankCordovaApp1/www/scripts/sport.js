﻿var sport, cal, dur, w, h, a, s;
var r;
$(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    var first = true;

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("sportLogDate").value = today;
});

document.getElementById("butSport").addEventListener("click", function () { r = document.getElementById('resultSport'); startRecognition(); }, false);
document.getElementById("butWeight").addEventListener("click", function () { r = document.getElementById('resultWeight'); startRecognition(); }, false);
document.getElementById("butHeight").addEventListener("click", function () { r = document.getElementById('resultHeight'); startRecognition(); }, false);
document.getElementById("butAge").addEventListener("click", function () { r = document.getElementById('resultAge'); startRecognition(); }, false);

function loadData() {
    var Ref1 = firebase.database().ref('settings/' + uID);
    Ref1.on('value', function (snapshot) {
        $("#resultSex").val(snapshot.val().gander); 
        document.getElementById('resultWeight').innerText = snapshot.val().weight;
        document.getElementById('resultHeight').innerText = snapshot.val().height;
        document.getElementById('resultAge').innerText = snapshot.val().age;

    });
}

function startRecognition() {
    window.plugins.speechRecognition.startListening(function (result) {
        // Show results in the console
        console.log(result);
        r.innerText = result[0];
    }, function (err) {
        console.error(err);
    }, {
            language: "en-IN",
            showPopup: true
        });
}
// Verify if recognition is available
window.plugins.speechRecognition.isRecognitionAvailable(function (available) {
    if (!available) {
        console.log("Sorry, not available");
    }
});


function validation() {
    if (isNaN(document.getElementById("resultHeight").value) == true || isNaN(document.getElementById("resultWeight").value) == true || isNaN(document.getElementById("resultAge").value) == true)
    {
        alert("Weight, Height and Age must be a numbers");
        return false;
    }

    else if (document.getElementById("resultHeight").value == "" || document.getElementById("resultWeight").value == "" || document.getElementById("resultAge").value == "" || document.getElementById("resultSport").value == "")
    {
        alert("Weight, Height and Age must have a values");
        return false;
    }

    else
        return true;
}

function getResult() {
    var nr = 0;
    $('.containerResult').html('');
    var settings = {
        async: true,
        crossDomain: true,
        url: "https://trackapi.nutritionix.com/v2/natural/exercise",
        method: "POST",
        headers: {
            "x-app-id": "dd913573",
            "x-app-key": "caa87c2d9d1e7bac2098983bb241b24d",
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
            "Postman-Token": "c280b3ed-417b-8378-9b0b-ca485f2f1eca"
        },
        processData: false,
        data: "{\r\n \"query\":\"" + document.getElementById("resultSport").value + "\",\r\n \"gender\":\"" + document.getElementById("resultSex").value + "\",\r\n \"weight_kg\":" + parseInt(document.getElementById("resultWeight").value) + ",\r\n \"height_cm\":" + parseInt(document.getElementById("resultHeight").value) + ",\r\n \"age\":" + parseInt(document.getElementById("resultAge").value)+"\r\n}"
    }

    $.ajax(settings).done(function (response) {

            var $newDiv = $('<div class="itemBar">' +
                '<h2>' + response.exercises[0].name + '<h2>' +
                '<h3>Calories: ' + response.exercises[0].nf_calories + '<h3>' +
                '<h3>Time: ' + response.exercises[0].duration_min + " " + "min" + '<h3>' +
                '<img width="150" height="150" src="' + response.exercises[0].photo.thumb + '">' +
                '</div>');

            $newDiv.attr("id", "div" + nr++);
            $('.containerResult').append($newDiv);
            sport = response.exercises[0].name;
            cal = response.exercises[0].nf_calories;
            dur = response.exercises[0].duration_min;
    });
}

function PostSport() {
    if (validation()) {
        var first = true;
        var today = document.getElementById('sportLogDate').value;
        var Ref1 = firebase.database().ref('sport/' + uID + '/' + today);
        var newPostKey = firebase.database().ref().child('sport').push().key;
        Ref1.on('value', function (snap) {
            
                firebase.database().ref('sport/' + uID + '/' + today + '/' + newPostKey).set({
                    sport: sport,
                    calories: cal,
                    duration: dur
            });
            if(first == true)
                alert("Saved succesfully");
            window.location.href = "home.html";     
            first = false;
        });
    }
}

function info(value) {
    if (value == "sport")
        alert("What did you do e.g: I run 3 miles");
    if (value == "weight")
        alert("Body weight");
    if (value == "height")
        alert("Body height");
    if (value == "age")
        alert("Your age");
    if (value == "sex")
        alert("Are you man or woman");
    if (value == "logDate")
        alert("Date when you want add the log");
}